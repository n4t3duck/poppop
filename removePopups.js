'use strict';

var page = document.location.host

if (page === 'www.businessinsider.com') {
    document.querySelector('.tp-modal').remove(); 
    document.querySelector('.tp-active').remove(); 
    document.querySelector('.tp-modal-open').classList.remove('tp-modal-open');
}

else if (page === 'www.independent.co.uk') {
        document.querySelector('.tp-modal').remove(); 
        document.querySelector('.tp-active').remove(); 
}