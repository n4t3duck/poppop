'use strict';

var rule1 = {
  conditions: [new chrome.declarativeContent.PageStateMatcher({
    pageUrl: {hostEquals: 'www.businessinsider.com'}
  })
  ],
      actions: [new chrome.declarativeContent.ShowPageAction()]
}

var rule2 = {
  conditions: [new chrome.declarativeContent.PageStateMatcher({
    pageUrl: {hostEquals: 'www.independent.co.uk'}
  })
  ],
      actions: [new chrome.declarativeContent.ShowPageAction()]
}

chrome.runtime.onInstalled.addListener(function() {
  chrome.declarativeContent.onPageChanged.removeRules(undefined, function() {
    chrome.declarativeContent.onPageChanged.addRules([rule1, rule2]);
  });
});